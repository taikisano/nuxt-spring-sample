package com.example.demo.entity;

import java.util.List;

import lombok.Data;

@Data
public class JobCategory {
    private int id;
    private String name;
    private List<Employee> employees;
}
