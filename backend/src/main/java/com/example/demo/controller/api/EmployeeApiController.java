package com.example.demo.controller.api;

import java.util.List;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/employees")
public class EmployeeApiController {
    
    @Autowired
    EmployeeService employeeService;

    @CrossOrigin
    @GetMapping
    public List<Employee> getEmployees() {
        List<Employee> employees = employeeService.findAll();
        return employees;
    }

    @CrossOrigin
    @GetMapping("{id}")
    public Employee getEmployee(@PathVariable Integer id) {
        Employee employee = employeeService.findById(id);
        return employee;
    }

    @CrossOrigin
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) // レスポンスを201番で
    public void postEmployee(@RequestBody Employee employee) {
        employeeService.insert(employee);
    }

    @CrossOrigin
    @PutMapping("{id}")
    public void putEmployee(@PathVariable Integer id, 
                            @RequestBody Employee employee) {
        employee.setId(id);
        employeeService.update(employee);
    }

    @CrossOrigin
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) // レスポンスを204番で
    public void deleteEmployee(@PathVariable Integer id) {
        employeeService.delete(id);
    }
}
