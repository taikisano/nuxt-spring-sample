package com.example.demo.mapper;

import java.util.List;

import com.example.demo.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper {
    List<Employee> findAll();
    Employee findById(int id);
    void insert(Employee employee);
    void update(Employee employee);
    void delete(int id);
}
