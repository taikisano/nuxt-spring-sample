use sample;

SET CHARACTER_SET_CLIENT = utf8;
SET CHARACTER_SET_CONNECTION = utf8;

create table job_categories (
    id char(1) not null primary key, 
    name varchar(32) ,
    created_at timestamp not null, 
    updated_at timestamp
);

create table employees (
    id int not null primary key auto_increment, 
    name varchar(32) , 
    job_category_id char(1), 
    created_at timestamp not null, 
    updated_at timestamp,
    FOREIGN KEY(job_category_id) 
    REFERENCES job_categories(id)
);

insert into job_categories (ID, NAME, CREATED_AT, UPDATED_AT) 
values (1, '会計職','2020-04-01', '2020-04-01');

insert into job_categories (ID, NAME, CREATED_AT, UPDATED_AT) 
values (2, '営業職','2020-04-01', '2020-04-01');

insert into job_categories (ID, NAME, CREATED_AT, UPDATED_AT) 
values (3, '開発職','2020-04-01', '2020-04-01');

insert into employees (ID, NAME, JOB_CATEGORY_ID, CREATED_AT, UPDATED_AT) 
values (1, 'tanaka','1','2020-04-01', '2020-04-01');

insert into employees (ID, NAME, JOB_CATEGORY_ID, CREATED_AT, UPDATED_AT) 
values (2, 'yamada','2','2020-04-01', '2020-04-01');

insert into employees (ID, NAME, JOB_CATEGORY_ID, CREATED_AT, UPDATED_AT) 
values (3, 'suzuki','2','2020-04-01', '2020-04-01');
